require 'sequel'
require 'sequel/plugins/serialization'

class Comentario < Sequel::Model
  many_to_one :evento



  plugin :json_serializer
  plugin :validation_helpers
  def validate
    super
    validates_presence [:texto, :fecha_hora, :evento_id, :usuario_login], :message=>'Dato requerido'
  end
end